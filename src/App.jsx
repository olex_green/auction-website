import React from 'react';
import { hot } from 'react-hot-loader';

import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';

import UserPageContainer from './pages/home/components/HomePageContainer';

const theme = createMuiTheme();

function App() {
  return (
    <MuiThemeProvider theme={theme}>
      <CssBaseline />
      <UserPageContainer />
    </MuiThemeProvider>
  );
}

export default hot(module)(App);
