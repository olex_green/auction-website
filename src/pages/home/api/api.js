/* eslint-disable import/prefer-default-export */
import axios from 'axios';

const API_BASE_URL = 'http://localhost:8003';

export function getItems() {
  return axios.get(`${API_BASE_URL}/items`);
}

export function sendBid(id, user, bid) {
  return axios.post(`${API_BASE_URL}/bid`, {
    id,
    user,
    bid,
  });
}
