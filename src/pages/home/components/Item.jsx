import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

import moment from 'moment';
import Avatar from '@material-ui/core/Avatar';
import AndroidIcon from '@material-ui/icons/Android';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import IconButton from '@material-ui/core/IconButton';
import AddBoxIcon from '@material-ui/icons/AddBox';
import AlarmIcon from '@material-ui/icons/Alarm';
import TextField from '@material-ui/core/TextField';
import InputAdornment from '@material-ui/core/InputAdornment';

const styles = {
  root: {
    paddingTop: 16,
    paddingBottom: 16,
  },
  avatar: {
    margin: 10,
    color: '#707070',
    backgroundColor: '#f5f5f5',
  },
  secondaryAction: {
    display: 'flex',
    '& > div': {
      alignSelf: 'center',
    },
  },
  priceInput: {
    width: 100,
  },
};

function Item({
  bid,
  classes,
  endTime,
  id,
  onChangeBid,
  price,
  sendBid,
  title,
  user,
}) {
  const sendBidHandler = () => {
    sendBid(id);
  };

  const leftTime = moment(endTime).diff(moment(), 'seconds');

  return (
    <TableRow key={id} className={classes.root}>
      <TableCell>
        <Avatar className={classes.avatar}>
          <AndroidIcon />
        </Avatar>
      </TableCell>
      <TableCell>{title}</TableCell>
      <TableCell>
        <IconButton disabled>
          <AlarmIcon />
        </IconButton>
      </TableCell>
      <TableCell>{`${leftTime < 0 ? 0 : leftTime} sec.`}</TableCell>
      <TableCell numeric>{user}</TableCell>
      <TableCell numeric>{price}</TableCell>
      <TableCell>
        <TextField
          disabled={leftTime < 0}
          name={id}
          onChange={onChangeBid}
          className={classes.priceInput}
          InputProps={{
            startAdornment: <InputAdornment position="start">+$</InputAdornment>,
            type: 'number',
          }}
          value={bid}
        />
      </TableCell>
      <TableCell>
        <IconButton onClick={sendBidHandler} disabled={leftTime < 0}>
          <AddBoxIcon />
        </IconButton>
      </TableCell>
    </TableRow>
  );
}

Item.propTypes = {
  classes: PropTypes.shape({
    root: PropTypes.string,
  }),
  bid: PropTypes.number,
  endTime: PropTypes.string,
  id: PropTypes.string,
  onChangeBid: PropTypes.func.isRequired,
  price: PropTypes.number,
  sendBid: PropTypes.func.isRequired,
  title: PropTypes.string,
  user: PropTypes.string,
};

Item.defaultProps = {
  bid: '',
  classes: {},
  endTime: '',
  id: '',
  price: 0,
  title: '',
  user: '',
};

export default withStyles(styles)(Item);
