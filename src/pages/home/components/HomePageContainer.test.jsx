import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';

import HomePage from './HomePage';
import HomePageContainer from './HomePageContainer';

function setup(props) {
  const enzymeWrapper = shallow(<HomePageContainer {...props} />);

  return {
    enzymeWrapper,
  };
}

describe('UserPageContainer', () => {
  it('should render self and has first HomePage', () => {
    const { enzymeWrapper } = setup();
    expect(enzymeWrapper.first().is(HomePage)).to.equal(true);
  });
});
