import React, { Component } from 'react';

import HomePage from './HomePage';

import * as api from '../api/api';

const SSE_HOST = 'http://localhost:8004';

class HomePageContainer extends Component {
  constructor() {
    super();
    this.state = {
      currentUser: '',
      isLogin: false,
      items: [],
    };
  }

  sendBid = async (id) => {
    const { currentUser, items } = this.state;

    const { bid } = items.find(item => item.id === id);
    try {
      await api.sendBid(id, currentUser, bid);
    } catch (e) {
      console.error(e);
    }
  }

  onChangeBid = (e) => {
    const { name, value } = e.target;
    if (value > 0) {
      this.setState((prevState) => {
        const { items } = prevState;
        const index = items.findIndex(item => item.id === name);

        if (index > -1) {
          items[index].bid = value;
        }

        return {
          items,
        };
      });
    }
  }

  onChangeUser = (e) => {
    const { value } = e.target;
    this.setState({ currentUser: value });
  }

  setUser = async () => {
    const { data } = await api.getItems();
    this.setState({ items: data, isLogin: true });
    const eventSource = new EventSource(SSE_HOST);

    eventSource.addEventListener('update', (e) => {
      if (!e.data) {
        return;
      }

      const {
        user,
        id,
        price,
      } = JSON.parse(e.data);

      this.setState((prevState) => {
        const { items } = prevState;
        const index = items.findIndex(item => item.id === id);

        if (index > -1) {
          items[index].price = price;
          items[index].user = user;
        }

        return {
          items,
        };
      });
    });
  }

  render() {
    const {
      currentUser,
      items,
      isLogin,
    } = this.state;

    return (
      <HomePage
        currentUser={currentUser}
        isLogin={isLogin}
        items={items}
        sendBid={this.sendBid}
        onChangeBid={this.onChangeBid}
        onChangeUser={this.onChangeUser}
        setUser={this.setUser}
      />
    );
  }
}

export default HomePageContainer;
