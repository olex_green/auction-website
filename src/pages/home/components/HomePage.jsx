import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

import Button from '@material-ui/core/Button';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TextField from '@material-ui/core/TextField';

import Header from '../../../common/Header';
import Item from './Item';

const styles = {
  root: {
    display: 'flex',
    height: 'calc(100vh - 64px)',
  },
  userBox: {
    display: 'flex',
    margin: 'auto',

    '& > button': {
      marginLeft: 10,
    },
  },
};

function HomePage({
  classes,
  currentUser,
  isLogin,
  items,
  onChangeBid,
  onChangeUser,
  sendBid,
  setUser,
}) {
  return (
    <React.Fragment>
      <Header title="Auction demo by Alex" user={currentUser} />
      <div className={classes.root}>
        {
          isLogin ? (
            <Table>
              <TableHead>
                <TableRow>
                  <TableCell />
                  <TableCell>Title</TableCell>
                  <TableCell />
                  <TableCell>Time Left</TableCell>
                  <TableCell>Last Bid: User</TableCell>
                  <TableCell numeric>Last Bid: Price ($)</TableCell>
                  <TableCell numeric>Add To Price</TableCell>
                  <TableCell />
                </TableRow>
              </TableHead>
              <TableBody>
                {
                  items.map(item => (
                    <Item {...item} sendBid={sendBid} onChangeBid={onChangeBid} />
                  ))
                }
              </TableBody>
            </Table>)
            : (
              <div className={classes.userBox}>
                <TextField label="User Name" value={currentUser} onChange={onChangeUser} />
                <Button variant="outlined" color="primary" onClick={setUser}>
                  Login
                </Button>
              </div>)
        }
      </div>
    </React.Fragment>
  );
}

HomePage.propTypes = {
  classes: PropTypes.shape({
    root: PropTypes.string,
  }),
  items: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.string,
    title: PropTypes.string,
    user: PropTypes.string,
    price: PropTypes.number,
    bid: PropTypes.number,
    endTime: PropTypes.string,
  })),
  currentUser: PropTypes.string,
  isLogin: PropTypes.bool,
  onChangeBid: PropTypes.func.isRequired,
  onChangeUser: PropTypes.func.isRequired,
  sendBid: PropTypes.func.isRequired,
  setUser: PropTypes.func.isRequired,
};

HomePage.defaultProps = {
  classes: {},
  currentUser: '',
  isLogin: false,
  items: [],
};

export default withStyles(styles)(HomePage);
