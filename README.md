# CODING CHALLENGE: Auction website by Alex Tkachuk

## Run whole solution:
- Run Web Application
- Run Node.js Application 

### Run Web Application
- npm i
- npm start
- open localhost:3004 in browser

## For starting the Auction again (reset time and prices)
- restart Node.js app
