const webpackMerge = require('webpack-merge');
const path = require('path');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');

const commonConfig = require('./webpack.config.base.js');
const entries = require('./webpack.entries');

module.exports = function prod() {
  return webpackMerge(commonConfig(false), {
    mode: 'production',
    entry: {
      main: path.resolve(__dirname, './src/index.jsx'),
      init: entries.init,
    },
    output: {
      path: `${__dirname}/dist`,
      filename: '[name].[hash].js',
      publicPath: '/static/',
    },
    optimization: {
      minimize: true,
      splitChunks: {
        chunks: 'all',
      },
      minimizer: [
        new UglifyJsPlugin({
          cache: true,
          parallel: true,
        }),
        new OptimizeCSSAssetsPlugin({}),
      ],
    },
  });
};
